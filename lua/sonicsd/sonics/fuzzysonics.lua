SonicSD:AddSonic({
    ID = "3rddocsonic",
    Name = "3rd Doctor Screwdriver",
    ViewModel = "models/fuzzyleo/sonics/c_3rdsonic.mdl",
    WorldModel = "models/fuzzyleo/sonics/w_3rdsonic.mdl",
    SoundLoop = "fuzzyleo/sonics/loop_2nd3rdsonic.wav",
    SoundLoop2 = "fuzzyleo/sonics/loop_seadevils3rd.wav",
    ButtonSoundOn = "sonicsd/button_on_2.wav",
    ButtonSoundOff = "sonicsd/button_off_3.wav",
    ButtonDelay = 0.1,
    DefaultLightColor = Color(160, 0, 0),
    DefaultLightColor2 = Color(160, 0, 0),
    DefaultLightColorOff = Color(250, 239, 0),
    LightDisabled = true,
    ModeSoundOn = "sonicsd/extend_1968.wav",
    ModeSoundOff = "sonicsd/retract_1968.wav",
    Animations = {
        Mode = {
            Param = "extend",
            Speed = 1.5
        },
        Toggle = {
            Param = "active",
            Speed = 1
        }
    }
})

SonicSD:AddSonic({
    ID = "5thdocsonic",
    Name = "5th Doctor Screwdriver",
    ViewModel = "models/fuzzyleo/sonics/c_5thsonic.mdl",
    WorldModel = "models/fuzzyleo/sonics/w_5thsonic.mdl",
    SoundLoop = "sonicsd/loop_1968_1.wav",
    SoundLoop2 = "sonicsd/loop_1968_2.wav",
    ButtonSoundOn = "sonicsd/button_on_2.wav",
    ButtonSoundOff = "sonicsd/button_off_3.wav",
    ButtonDelay = 0.1,
    DefaultLightColor = Color(160, 0, 0),
    DefaultLightColor2 = Color(160, 0, 0),
    DefaultLightColorOff = Color(255, 255, 255),
    LightDisabled = true,
    ModeSoundOn = "sonicsd/extend_1968.wav",
    ModeSoundOff = "sonicsd/retract_1968.wav",
    Animations = {
        Mode = {
            Param = "extend",
            Speed = 1.5
        },
        Toggle = {
            Param = "active",
            Speed = 1
        }
    }
})